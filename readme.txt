In order to install and configure the module, do following:
- download the module
- unzip and put the module folder in the folder sites/modules.
- go to Administer -> Site building -> Modules and enable the module
- go to Administer->Site configuration->Area Banner

There you can select a default banner. This banner will be displayed for all menu items (pages) that don't have an own banner defined.
You can also define a banner for a specific menu item by selecting the menu item from the drop down list and uploading a banner file. The banner will then be displayed for this menu item and it's children.
You can upload two types of banner files,  images and flash animations.